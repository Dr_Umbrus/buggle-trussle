﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FruitQuake : MonoBehaviour
{
    GameManager gm=null;
    public GameObject shadow= null;
    public GameObject[] powerUp = new GameObject[0];
    public float timer=0;
    public float maxTimer=0;
    public int variante=0;

    [SerializeField]
    float wormTime = 0, maxWormTime = 0;

    [SerializeField]
    GameObject wormPart = null;


    private void Awake()
    {
        timer = maxTimer;
        gm = FindObjectOfType<GameManager>();
    }

    private void Update()
    {
        timer -= Time.deltaTime;
        wormTime -= Time.deltaTime;
        if (timer <= 0)
        {
            timer = 999;
            StartCoroutine(Quaking());
        }
        if (wormTime <= 0)
        {
            wormTime = maxWormTime + Random.Range(-5, 6);
            int rand = Random.Range(0, 4);
            Debug.Log(rand*90);
            
            StartCoroutine(Worming(rand*90));
        }
    }

    IEnumerator Worming(int angle)
    {
        for (int i = 0; i < 25; i++)
        {
            Instantiate(wormPart, transform.position, Quaternion.Euler(0, 0, angle));
            yield return new WaitForSeconds(0.1f);
        }
    }

    IEnumerator Quaking()
    {
        int rocks = Random.Range(7,13);

        gm.Shake(5, 35, 1.05f);
        for (int i = 0; i < rocks; i++)
        {
            Vector3 pos = Vector3.zero;
            pos.z = -2;
            pos.x = Random.Range(-gm.width, gm.width +0.1f);
            pos.y = Random.Range(-gm.height, gm.height -0.5f);
            int rand = Random.Range(0, powerUp.Length);
            GameObject temp = Instantiate(shadow,pos, Quaternion.identity);
            temp.GetComponent<Fruitfall>().powerup = powerUp[rand];
            yield return new WaitForSeconds(Random.Range(0.1f,0.35f));
        }

        timer = maxTimer + Random.Range(-variante, variante + 1) / 10;
    }
}
