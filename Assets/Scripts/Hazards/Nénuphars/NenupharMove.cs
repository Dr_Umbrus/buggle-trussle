﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NenupharMove : MonoBehaviour
{
    Vector3 startPos, endPos;
    float timerL = 0, timeSink = 0;
    bool moveIn = false;
    public GameObject waterPart = null;
    CircleCollider2D col;
    List<Character> underwater = new List<Character>();
    List<float> underwatime = new List<float>();


    private void Awake()
    {
        col = GetComponent<CircleCollider2D>();
        startPos = transform.position;
        endPos = startPos;
        endPos.z += 1;
    }

    private void Update()
    {
        if (!moveIn)
        {
            timerL -= Time.deltaTime;
        }
        else
        {
            timerL += Time.deltaTime;
        }

        transform.position = Vector3.Lerp(startPos, endPos, timerL);

        if (timeSink > 0)
        {
            timeSink -= Time.deltaTime;

            if (timeSink <= 0)
            {
                timerL = 1;
                moveIn = false;

            }
        }

        if (timerL >= 0.5f)
        {
            col.enabled = true;
        }
        else
        {
            col.enabled = false;
        }
    }

    public void MoveIn()
    {
        timeSink = 10;
        moveIn = true;
        timerL = 0;
        //Instantiate(waterPart, transform.position, Quaternion.identity);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<Character>() != null)
        {
            bool test = true;
            if (underwater.Count > 0)
            {
                for (int i = 0; i < underwater.Count; i++)
                {
                    if (underwater[i] = collision.GetComponent<Character>())
                    {
                        test = false;
                    }
                }
            }
            if (test)
            {
                underwater.Add(collision.GetComponent<Character>());
                underwatime.Add(0.2f);
            }

        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.GetComponent<Character>() != null)
        {
            Character potato = collision.GetComponent<Character>();
            int test = -1;
            for (int i = 0; i < underwater.Count; i++)
            {
                if (potato == underwater[i])
                {
                    test = i;
                    underwatime[i] -= Time.deltaTime;
                    if (underwatime[i] <= 0)
                    {
                        underwatime[i] = 0.2f;
                        potato.Damage(1, -1, true, false);
                    }
                }
            }

            if (test < 0)
            {
                underwater.Add(collision.GetComponent<Character>());
                underwatime.Add(0.2f);
            }
        }

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.GetComponent<Character>() != null)
        {
            Character potato = collision.GetComponent<Character>();
            int test = -1;
            for (int i = 0; i < underwater.Count; i++)
            {
                if (potato == underwater[i])
                {
                    test = i;

                }
            }

            if (test >= 0)
            {
                underwater.RemoveAt(test);
                underwatime.RemoveAt(test);
            }
        }
    }

}
