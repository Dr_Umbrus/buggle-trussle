﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damageable : MonoBehaviour
{
    public int life = 0;
    
    public AudioSource aud;
    public AudioClip ded;
    public bool dying=false;
    public bool fixe=false;
    public Rigidbody2D rb;
    public bool speDead = false;
    public GameManager gm = null;
    public int id = -1;

    public void Awake()
    {
        aud = GetComponent<AudioSource>();
        aud.volume = 0.8f;
        aud.spatialBlend = 1;
        aud.minDistance = 8;
        aud.maxDistance = 20;
        aud.dopplerLevel = 0;
        rb = GetComponent<Rigidbody2D>();
        gm = FindObjectOfType<GameManager>();
        Awakening();
    }

    public virtual void Awakening()
    {

    }

    public void Damage(int dam, int source, Vector2 force, bool go, bool lifeSteal)
    {
        if (lifeSteal)
        {
            gm.team[source].HealMe(Mathf.CeilToInt(dam / 2));
        }

        if (!fixe)
        {
            Vector2 targetSens = force;
            targetSens.Normalize();
            rb.velocity = targetSens * rb.velocity.magnitude;
            rb.velocity += force;
        }
        life -= dam;

        if (life <= 0 && !dying)
        {
            dying = true;
            Death(source);
            StartCoroutine(Dying());
        }
        if (!dying)
        {
            Dam(source);
        }   
    }

    public virtual void Dam(int source)
    {

    }

    public virtual void Death(int source)
    {

    }

    public IEnumerator Dying()
    {
       
        GetComponent<SpriteRenderer>().color = Color.clear;
        GetComponent<Collider2D>().enabled = false;
        aud.clip = ded;
        aud.Play();
        while (aud.isPlaying)
        {
            yield return 0;
        }
        yield return 0;
        if (!speDead)
        {
            Destroy(gameObject);
        }
        
    }

}
