﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoomerangShot : Projectile
{
    Character owner = null;
    GameObject creator;
    public Vector3 target, spawn;
    float timer = 0;
    bool retour=false;
    public bool started = false;
    public float dist;


    public override void Starting()
    {
        StartCoroutine(TargetAcquired());
    }

    IEnumerator TargetAcquired()
    {
        yield return 0;
        
        
        Character[] potato = FindObjectsOfType<Character>();
        foreach (Character dude in potato)
        {
            if (dude.playerNum == player)
            {
                creator = dude.gameObject;
                owner = dude;
            }
        }

        spawn = creator.transform.position;

        Vector3 aim = new Vector3(transform.position.x - spawn.x, transform.position.y - spawn.y, 0);
        aim.Normalize();
        aim *= dist;
        target = (transform.position + aim);

        spawn = transform.position;
        started = true;
    }

    public override void Pattern()
    {
        
        if (started)
        {

            if (owner.life <= 0)
            {
                Destroy(gameObject);
            }

            transform.Rotate(new Vector3(0, 0, 20));
            transform.position = Vector3.Lerp(spawn, target, timer);
            timer += Time.deltaTime*speed;
            if (retour)
            {
                target = creator.transform.position;
            }

            if (timer >= 1)
            {
                if (!retour)
                {
                    Retour();
                }
                else
                {


                    Destroy(gameObject);
                }
            }
        }
    }

    public void Retour()
    {
        spawn = transform.position;
        retour = true;
        timer = 0;
    }



    

}
