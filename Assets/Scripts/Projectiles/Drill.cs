﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drill : Projectile
{
    public Sprite activeSprite;

    public override void Pattern()
    {
        //Il va en ligne droite
        transform.Translate((Vector3.up * speed * Time.deltaTime), Space.Self);
    }

    public override void Touch(Collider2D collision)
    {
        damage += 5;
        GetComponent<SpriteRenderer>().sprite = activeSprite;
        GetComponent<SpriteRenderer>().color = Color.red;
    }

    public override void HitPlayer(Collider2D collision)
    {
        StartCoroutine(Dying());
    }
}
