﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nautilus : Character
{

    /// <summary>
    /// Le perso à grab
    /// </summary>

    public GameObject anchor;
    GameObject summonedAnchor;


    public override void Pattern()
    {
        if (summonedAnchor != null)
        {
            stopped = true;
        }
    }

    public override void Competence()
    {
        summonedAnchor = Instantiate(anchor, transform.position, Quaternion.identity);
        summonedAnchor.transform.rotation = Quaternion.Euler(0, 0, angle);
        summonedAnchor.transform.Rotate(new Vector3(0, 0, -90));
        summonedAnchor.GetComponent<Projectile>().Create(playerNum, false);
    }

    public void EndAnchor()
    {
        summonedAnchor = null;
        stopped = false;
    }
}
