﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Anchor : Projectile
{
    public float stunTime = 0;
    public float lerpTime = 0;
    Transform creator = null;
    Transform victime = null;
    bool grabbed = false, grabChar = false;
    Vector2 grabPos = Vector2.zero;
    Vector2 creatorStart = Vector2.zero, victimeStart = Vector2.zero;

    public override void Pattern()
    {
        if (!grabbed)
        {
            transform.Translate((Vector3.up * speed * Time.deltaTime), Space.Self);
        }
        else
        {
            lerpTime += Time.deltaTime * 2;

            creator.position = Vector2.Lerp(creatorStart, grabPos, lerpTime);

            if (grabChar)
            {
                victime.position = Vector2.Lerp(victimeStart, grabPos, lerpTime);
            }

            if (lerpTime >= 1)
            {
                
            }
        }
    }

    void KillMe()
    {
        creator.GetComponent<Nautilus>().EndAnchor();
        Destroy(gameObject);
    }

    public override void Touch(Collider2D collision)
    {
        grabbed = true;
        creatorStart = creator.position;
        grabPos = transform.position;
    }

    public override void HitPlayer(Collider2D collision)
    {
        grabbed = true;
        creatorStart = creator.position;
        
        grabChar = true;
        victime = collision.transform;
        victime.GetComponent<Character>().Stunned(stunTime);
        victimeStart = victime.transform.position;

        grabPos = new Vector2((creatorStart.x+victimeStart.x)/2, (creatorStart.y + victimeStart.y) / 2);
    }
}
